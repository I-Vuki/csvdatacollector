export class CsvRecord {
    public ime: string;
    public prezime: string;
    public post_br: string;
    public grad: string;
    public telefon: string;
    public err: boolean;
  }