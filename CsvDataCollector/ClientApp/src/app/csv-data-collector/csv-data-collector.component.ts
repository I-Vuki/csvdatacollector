import { Component, ViewChild, Inject } from '@angular/core';  
//import { CsvRecord } from './data';  
import { isNumber } from 'util';
import { HttpClient } from '@angular/common/http';
import { FileService } from './../file.service'
  
@Component({  
  selector: 'app-csv-data-collector-component',
  templateUrl: './csv-data-collector.component.html',
  styleUrls: ['./csv-data-collector.component.css']  
})  

export class CsvDataCollectorComponent {  
  title = 'CSV-CollectData';  

  public returnMessage: any;

  public http: HttpClient;
  public baseURL:string;

  public records: CsvRecord[] = [];
  @ViewChild('csvReader', {static: false}) csvReader: any;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private FileService:FileService){
    this.http = http;
    this.baseURL = baseUrl;
  }

  saveFunction(): void{
    console.log(this.records);
    this.http.post<any>(this.baseURL + 'person', this.records).subscribe(result => {
      this.returnMessage = result;
    }, error => console.error(error));
  }
  
  getFile(): void {  
  
    this.FileService.getText().subscribe(data => {

      let csvRecordsArray = (data).split(/\r\n|\n/);
      this.records = this.getDataRecords(csvRecordsArray);

    });
  }

  uploadListener(event): void {  
  
    let text = [];  
    let files = event.srcElement.files;  

    if (this.isValidCsvFile(files[0])) {  
  
      let input = event.target;  
      let reader = new FileReader();  
      console.log(input);
      reader.readAsText(input.files[0]);
  
      reader.onload = () => {  
        let csvData = reader.result;  
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  

        this.records = this.getDataRecords(csvRecordsArray);  
      };
  
      reader.onerror = () => {
        console.error('Error occured while reading the file!');  
      };
  
    } else {  
      alert("Please import a valid .csv file.");  
      this.fileReset();  
    }
  }
  
  getDataRecords(csvRecordsArray: any) {  
    let csvArr = [];  
  
    for (let i = 0; i < csvRecordsArray.length; ++i) {

      let curruntRecord = (<string>csvRecordsArray[i]).split(';');
      let tmpRecord: CsvRecord = new CsvRecord();

      tmpRecord.ime = curruntRecord[0];   
      tmpRecord.prezime = curruntRecord[1];
      tmpRecord.postanskiBr = curruntRecord[2];
      tmpRecord.grad = curruntRecord[3];   
      tmpRecord.brojTelefona = curruntRecord[4];

      if(isNumber(+curruntRecord[2]) && (curruntRecord[2]+"").length == 5 ){
        tmpRecord.err = false;
      }else{
        tmpRecord.err = true;
      }

      csvArr.push(tmpRecord);  
    }
    return csvArr;  
  }
  
  isValidCsvFile(file: any) {  
    return file.name.endsWith(".csv");  
  }
  
  fileReset() {  
    this.csvReader.nativeElement.value = "";  
    this.records = [];  
  }  
}
class CsvRecord {
  public ime: string;
  public prezime: string;
  public postanskiBr: string;
  public grad: string;
  public brojTelefona: string;
  public err: boolean;
}
interface IReturnMsg{
  message : string;
}