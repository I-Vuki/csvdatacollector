import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
//Read  text file
export interface ResultJson{

}
@Injectable({
  providedIn: 'root'
})
export class FileService {
  urlEncoded = 'assets/app/files/podaci.csv';

  constructor(private http: HttpClient) {
    this.http = http;
  }

  getText(){
    return this.http.get(this.urlEncoded, {responseType: 'csv' as 'text'});
  }
}