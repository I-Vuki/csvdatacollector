import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { CsvDataCollectorComponent } from './csv-data-collector/csv-data-collector.component';
import { FileService } from './file.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CsvDataCollectorComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: CsvDataCollectorComponent, pathMatch: 'full' },
      { path: 'csv-data-collector', component: CsvDataCollectorComponent },
    ])
  ],
  providers: [FileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
