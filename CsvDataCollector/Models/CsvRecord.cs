using System;

namespace CsvDataCollector.Models
{
    public class CsvRecord
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string PostanskiBr { get; set; }
        public string Grad { get; set; }
        public string BrojTelefona { get; set; }
        public bool Err { get; set; }
    }
    public class ReturnMessage
    {
        public string Message { get; set; }
    }
}