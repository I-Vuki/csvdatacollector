using System;
using System.ComponentModel.DataAnnotations;

namespace CsvDataCollector.Models
{
    public class Person
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string PostanskiBr { get; set; }
        public string Grad { get; set; }

        [Key]
        public string BrojTelefona { get; set; }
    }
}