using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CsvDataCollector.Data;
using CsvDataCollector.Models;

namespace CsvDataCollector.Controllers
{   
    [ApiController]
    [Route("[controller]")]
    public class PersonController : Controller
    {
        private readonly CsvDataCollectorContext _context;

        public PersonController(CsvDataCollectorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public ReturnMessage Post([FromBody]CsvRecord[] records)
        {
            String returnMessage = "Data was saved successfully";
            var duplicates = new List<Person>();
            var ret = new ReturnMessage();

            if(records.Length <= 0)
            {
                returnMessage = "No records have been sent";
                
                ret.Message = returnMessage;
                return ret;
            }

            foreach (var record in records)
            {
                Person person = new Person();
                if(!record.Err)
                {
                    person.Ime = record.Ime;
                    person.Prezime = record.Prezime;
                    person.PostanskiBr = record.PostanskiBr;
                    person.Grad = record.Grad;

                    if(record.BrojTelefona != "")
                    {
                        person.BrojTelefona = record.BrojTelefona;
                    }
                    else
                    {
                        returnMessage = "Telephone Number is needed for each record";
                    }

                    var duplicate = _context.Person.Find(person.BrojTelefona);
                    
                    if(duplicate == null)
                    {
                        _context.Add(person);
                        _context.SaveChangesAsync();
                    }
                    else
                    {
                        duplicates.Add(duplicate);
                    }

                }
            }
            if(duplicates.Count > 0)
            {
                if(returnMessage == "Data was saved successfully")
                {
                    returnMessage = "The following records already existed in the database: \n";
                }
                else
                {
                    returnMessage += "\nThe following records already existed in the database: \n";
                }

                foreach(var duplicate in duplicates)
                {
                    returnMessage += duplicate.Ime + " " + duplicate.Prezime + " " + duplicate.PostanskiBr + " " + duplicate.Grad + " " + duplicate.BrojTelefona +"\n";
                }
            }

            // Console.WriteLine(returnMessage);
            ret.Message = returnMessage;
            return ret;
        }
    }

}
