using Microsoft.EntityFrameworkCore;
using CsvDataCollector.Models;

namespace CsvDataCollector.Data
{
    public class CsvDataCollectorContext : DbContext
    {
        public CsvDataCollectorContext(DbContextOptions<CsvDataCollectorContext> options)
            :base(options)
        {
        }

        public DbSet<Person> Person { get; set; }
    }
}